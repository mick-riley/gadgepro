from django.contrib import admin
from .models import Driver, Customer, Journey, Address, Vehicle,Dispatcher,DriverRequest
from django.contrib.auth.admin import UserAdmin

# Register your models here.
admin.site.register(Dispatcher, UserAdmin)
admin.site.register(Address)
admin.site.register(Vehicle)
admin.site.register(Driver)
admin.site.register(Customer, UserAdmin)
admin.site.register(Journey)
admin.site.register(DriverRequest)