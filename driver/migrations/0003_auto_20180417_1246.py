# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2018-04-17 17:46
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0008_alter_user_username_max_length'),
        ('driver', '0002_auto_20180416_1427'),
    ]

    operations = [
        migrations.CreateModel(
            name='Dispatcher',
            fields=[
                ('user_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, to=settings.AUTH_USER_MODEL)),
                ('employee_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='driver.Employee')),
            ],
            options={
                'verbose_name': 'Dispatcher',
            },
            bases=('driver.employee', 'auth.user'),
        ),
        migrations.AlterModelOptions(
            name='address',
            options={'verbose_name_plural': 'Addresses'},
        ),
        migrations.RemoveField(
            model_name='employee',
            name='id',
        ),
        migrations.AlterField(
            model_name='employee',
            name='e_id',
            field=models.IntegerField(primary_key=True, serialize=False),
        ),
    ]
