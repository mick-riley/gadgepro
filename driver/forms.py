from django.contrib.auth.models import User
from django import forms
from .models import *

class DispatcherForm(forms.ModelForm):
	password = forms.CharField(widget=forms.PasswordInput)	

	class Meta:
		model = User
		fields = ['username','email','password']

class UserForm(forms.ModelForm):
	password = forms.CharField(widget=forms.PasswordInput)	

	class Meta:
		model = User
		fields = ['username','email','password']


class DriverRequestForm(forms.ModelForm):

	class Meta:
		model = DriverRequest
		fields = []

class JourneyForm(forms.ModelForm):
    
	class Meta:
		model = Journey
		fields = '__all__'
