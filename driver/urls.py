"""gadgepro URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from . import views

app_name = 'driver'

urlpatterns = [
    url(r'^dispatcher_form/',views.DispatcherFormView.as_view(),name='dispatcher_form'),
    url(r'^$', views.index, name='index'),
    #url(r'^driver/create',views.DriverCreate.as_view(),name='create'),
    url(r'^user_form/', views.UserFormView2.as_view(), name='index'),
    url(r'^driver_requests/', views.RequestViewer.as_view(), name='index'),
    url(r'^driver_request_form/', views.DriverRequestFormView.as_view(), name='index'),
    url(r'^customerRequest/',views.CustomerCreate.as_view(),name='customer_create'),
    url(r'^customerlogin/',views.UserFormView.as_view(),name='customer_login')

]
