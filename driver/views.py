from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django.core.urlresolvers import reverse_lazy
from django.views import generic

from django.views.generic.edit import CreateView, FormView


from .models import *
from django.contrib.auth import authenticate, login
from django.views.generic import View
from .forms import UserForm, DriverRequestForm, JourneyForm


# Create your views here.
def index(request):
    return render(request, 'driver/index.html')


class DispatcherFormView(FormView):
    template_name = 'driver/dispatcher_form.html'
    form_class = UserForm
    success_url = '/thanks/'

    def form_valid(self, form):
        return super().form_valid(form)


class CustomerCreate(CreateView):
    model = Customer
    fields = '__all__'
    template_name = 'driver/customer_form.html'
    success_url = reverse_lazy('driver:index')

    def form_valid(self, form):
        return super().form_valid(form)


class UserFormView(FormView):
    template_name = 'driver/user_form.html'
    form_class = UserForm
    success_url = '/details'


class UserFormView2(FormView):
    template_name = 'driver/user_form.html'
    form_class = UserForm
    success_url = '/thanks/'

    def form_valid(self, form):

        return super().form_valid(form)


class DriverRequestFormView(FormView):
    template_name = 'driver/user_form.html'
    form_class = DriverRequestForm
    success_url = '/thanks/'

    def get(self, request, *args, **kwargs):
        form = self.form_class(initial=self.initial)
        journey_form = JourneyForm()

        return render(request, self.template_name, {'form': form, 'journey_form': journey_form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            # create journey

            journey = Journey(currentLocation=request.POST.get('currentLocation', ''), destination=request.POST.get('destination', ''),
                              noOfPeople=int(request.POST.get('noOfPeople', '')), cost=int(request.POST.get('noOfPeople', '')),)

            
            journey.save()

            if Customer.objects.filter(username=request.user.username).exists():
                user = Customer.objects.get(username=request.user.username)
                print(type(request.user))
                driver_request = DriverRequest(
                    journey=journey, customer=user,)

                driver_request.save()
                return HttpResponse('success')
            else:
                return HttpResponse('You must be a customer')

        return render(request, self.template_name, {'form': form})


class RequestViewer(View):
    template_name = 'driver/requests.html'

    def get(self, request, *args, **kwargs):
        requests = DriverRequest.objects.filter(state="DECLINED")

        return render(request, self.template_name, {'requests': requests, })

    def post(self, request, *args, **kwargs):
        print(request.POST)
        drequest = DriverRequest.objects.get(id=request.POST['request_id'])
        drequest.state = request.POST.get("rstate")
        drequest.save()

        return HttpResponse("Successfully changed request to approved")
